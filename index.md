---
layout: default
title: Daily Report
---

<section>

# Daily report collection for public ClearCode work.

</section>

<section>

## Reports

  * benson
    * [benson/2022-01]({% link benson/2022-01.md %})
    * [benson/2021-12]({% link benson/2021-12.md %})
    * [benson/2021-11]({% link benson/2021-11.md %})
    * [benson/2021-10]({% link benson/2021-10.md %})
    * [benson/2021-09]({% link benson/2021-09.md %})
  * kou
    * [kou/2021-12]({% link kou/2021-12.md %})
    * [kou/2021-11]({% link kou/2021-11.md %})
    * [kou/2021-10]({% link kou/2021-10.md %})
    * [kou/2021-09]({% link kou/2021-09.md %})

</section>

<section>

## How to start writing a report?

Create your directory:

```bash
mkdir -p ${USER}
```

Copy `template.md`:

```bash
cp template.md ${USER}/$(date +%Y-%m).md
```

Add your entry to `index.md`:

```bash
editor index.md
```

Sample entry:

```markdown
{% raw %}
...
## Reports
...
  * ${USER}
    * [${USER}/YYYY-MM]({% link ${USER}/YYYY-MM.md %})
...
{% endraw %}
```

</section>

<section>

## How to write a daily report?

Fill items in template:

```bash
editor ${USER}/$(date +%Y-%m).md
```

Commit and push your change:

```bash
git add ${USER}/$(date +%Y-%m).md
git commit
git push
```

</section>

<section>

## License

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

See commit log for the author information.

</section>



