---
layout: default
title: 2021-10
---

<section>

## 2021-10-29

### What was done

  * Couldn't work because I need to do other work.

    * TODO: Review https://github.com/apache/arrow/pull/11562

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-28

### What was done

  * Couldn't work because I need to do other work.

    * Need to prepare a slide for https://www.db-tech-showcase.com/2021/ .
      Dead line: 2021-11-05.

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-27

### What was done

  * Couldn't work because I need to do other work.

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-26

### What was done

  * Couldn't work because I need to do other work.

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-25

### What was done

  * Couldn't work because I need to do other work.

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-22

### What was done

  * Couldn't work because I need to do other work.

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-21

### What was done

  * Couldn't work because my child feels sick.

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-20

### What was done

  * Couldn't work because my child feels sick.

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-19

### What was done

  * Reviewed, commented and merged

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * We should keep green CI for smooth release...

### Etc

  * ...

</section>

<section>

## 2021-10-18

### What was done

  * Reviewed and commented

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-15

### What was done

  * Reviewed and commented

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * zlib problem is fixed: https://github.com/ruby/zlib/pull/32

</section>

<section>

## 2021-10-14

### What was done

  * Reviewed and commented

### Good things

  * There is a person who is interested in Ruby bindings of DataFusion.
    https://gitter.im/red-data-tools/en?at=61677ba98c019f0d0b33291c

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-13

### What was done

  * Forgot to review https://github.com/apache/arrow/pull/11231 ...

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-12

### What was done

  * Reviewed some Benson's PRs and issues
  * All ClearCode members introduced their OSS related work to Benson

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * Auto collecting commented PRs and issues

### Etc

  * ...

</section>

<section>

## 2021-10-11

### What was done

  * Reviewed some Benson's PRs
  * Meeting

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-08

### What was done

  * Reviewed some Benson's PRs
    * I didn't know why Benson looked into string comparison including
      Japanese text support. I realize that it's for
      `between("STRING", "STRING_LEFT", "STRING_RIGHT")` today.

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * I should write this morning. I forget many things evening...

### Etc

  * My recent (difficult...) Ruby commiter work:
    * https://github.com/ruby/csv/pull/221
    * https://github.com/ruby/zlib/pull/32

</section>

<section>

## 2021-10-07

### What was done

  * Forgot to write this...

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>

<section>

## 2021-10-06

### What was done

  * Working on other tasks (Groonga, PGroonga and so on)
  * Commented on https://github.com/apache/arrow/pull/11275

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * I should work on Apache Arrow related things between 13:00-18:00
    JST not 06:00-10:00 JST.

### Etc

  * ...

</section>

<section>

## 2021-10-05

### What was done

  * Commented some Benson's work
    * TODO: Commented on https://issues.apache.org/jira/projects/ARROW/issues/ARROW-6407
  * ...

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * I should have time to write this...

</section>

<section>

## 2021-10-04

### What was done

  * Merged Benson's pull request: https://github.com/apache/arrow/pull/11275
  * ...

### Good things

  * ...

### Clarifications and possible problems

  * Should I write Apache Arrow related work that doesn't related to
    Benson's work here?
    * It's for easy to see my work (ClearCode work) from Benson to
      evaluate ClearCode by Benson.
    * e.g.
      * https://github.com/apache/arrow/pull/11300
      * https://github.com/apache/arrow/pull/11304
      * https://lists.apache.org/thread.html/r29b7e2adf6adc267c610f5b9b7f9d3749659be02f6934a787ed5c046%40%3Cdev.arrow.apache.org%3E

### Ideas to improve something

  * ...

### Etc

  * I should have time to write this...

</section>

<section>

## 2021-10-01

### What was done

  * Couldn't work because my child feels sick.

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

</section>
