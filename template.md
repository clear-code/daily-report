---
layout: default
title: YYYY-MM
---

<section>

## YYYY-MM-DD

### What was done

  * ...

### Good things

  * ...

### Clarifications and possible problems

  * ...

### Ideas to improve something

  * ...

### Etc

  * ...

## YYYY-MM-${DD - 1}

...

</section>

