#!/usr/bin/env ruby
# A Ruby script to get information on
# Apache Arrow related activity and generate
# a diary entry template for activity over the
# last 24 hours.
# related to Apache Arrow of a person with
# Run it using
# > ruby LogTemplate.rb githubusername
# where githubusername is the GitHub username
# the activity is associated with.

require 'json'
require 'net/http'
require 'time'

def getactivity(githubuser)
  # Use GitHub REST API to get information on Arrow related activity
  # https://docs.github.com/en/rest/guides/getting-started-with-the-rest-api#repositories
  file = "https://api.github.com/users/" + githubuser + "/events"
  response = Net::HTTP.get_response(URI.parse(file))
  activity = response.body
  activity = JSON.parse(activity)

  activity_info =[]
  activity.each do |item|
    repository = item["repo"]["name"]
    if repository.include? "arrow"
      created_at = item["created_at"]
      activity_time = Time.strptime(created_at, "%Y-%m-%dT%k:%M:%SZ")
      if activity_time > ( Time.now.utc - 24*60*60 )
        entry = (item["type"].gsub(/[A-Z]/) { |letter| ' '+letter}).downcase
        entry = entry.strip.capitalize
        entry << " GitHub repository [#{repository}]"
        entry << "(https://github.com/#{repository})"
        entry << " at #{created_at}."
        if item["type"].include? "CommentEvent"
          entry << "\n"
          entry << item["payload"]["comment"]["html_url"]
          entry << "\n"
          entry << <<~BODY
            `````markdown
            #{item["payload"]["comment"]["body"]}
            `````
          BODY
        end
        activity_info.push(entry)
      end
    end
  end
return activity_info
end

username = ARGV[0].to_s
activity_list = getactivity(username)
time = Time.new
puts "<section>"
puts ""
puts "## "+time.strftime("%Y-%m-%d")
puts ""
puts "### What was done"
puts ""
if activity_list.empty?
  puts "  * ..."
else
  activity_list.each do |item|
    item.each_line.with_index do |line, i|
      if i.zero?
        puts "  * #{line}"
      else
        puts "    #{line}"
      end
    end
  end
end
puts ""
puts "### Good things"
puts ""
puts "  * ..."
puts ""
puts "### Clarifications and possible problems"
puts ""
puts "  * ..."
puts ""
puts "### Ideas to improve something"
puts ""
puts "  * ..."
puts ""
puts "### Etc"
puts ""
puts "  * ..."
puts ""
puts "</section>"
