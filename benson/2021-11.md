---
layout: default
title: 2021-11
---

<section>

## 2021-11-30

### What was done
 
   * Verification script seems to work 
     [pull request](https://github.com/apache/arrow/pull/11735)
   * Formatting fixes 
     [pull request](https://github.com/apache/arrow/pull/11785)
     was merged

### Good things

   * ... 
   
### Clarifications and possible problems

   * ... 

### Ideas to improve something

   * ... 

### Etc
   
   *  Interesting distributed database
      [paper](http://www.cs.utah.edu/~lifeifei/papers/memdisagg-vldb21.pdf) 

</section>


<section>

## 2021-11-29

### What was done
 
   * Partial updates on 
     [pull request](https://github.com/apache/arrow/pull/11735)
     to improve release verificaiton script 
   * Formatting fixes for between kernel
     [pull request](https://github.com/apache/arrow/pull/11337)
   * Raised follow on 
     [issue](https://issues.apache.org/jira/browse/ARROW-14900) 
     for between kernel. Maybe reformat to sub issue?

### Good things

   * ... 
   
### Clarifications and possible problems

   * Need to figure out why compute.py had linting problem
     and check if merged incorrect branch by mistake

### Ideas to improve something

   * ... 

### Etc
   
   *  Gave example of Aozora Bunko as an approach to 
      preserve heritage at DHASA talk

</section>


<section>

## 2021-11-25

### What was done
 
   * Need to automate collection of relevant GitHub and GitLab activity 
   * Suggested Ruby section for 6.0.1 release notes

### Good things

   * Alibaba cloud has discount on VMs
   * Some website owners can be responsive
   
### Clarifications and possible problems

   * ... 

### Ideas to improve something

   * Find stable ideally archivable location for Red Datasets
     to make maintenance easier

### Etc
   
   * Small update to Red Datasets pull request

</section>


<section>

## 2021-11-24

### What was done
 
   * ... 

### Good things

   * ... 
   
### Clarifications and possible problems

   * ... 

### Ideas to improve something

   * ... 

### Etc
   
   * [Fix failing Red datasets test](https://github.com/red-data-tools/red-datasets/pull/112)
   * [Add Diamonds dataset to Red datasets](https://github.com/red-data-tools/red-datasets/pull/110)

</section>


<section>

## 2021-11-23

### What was done
 
   * [Issue](https://issues.apache.org/jira/browse/ARROW-14801),
     to automate Javascript release candidate testing. 

### Good things

   * ... 
   
### Clarifications and possible problems

   * ... 

### Ideas to improve something

   * ... 

### Etc
   
   * ... 

</section>


<section>

## 2021-11-22

### What was done
 
   * Tried debugging R build problem
     [issue](https://issues.apache.org/jira/browse/ARROW-14786),
     was not able to solve it. Separate pull request has been
     made to solve issue by Neal Richardson
   * [Pull request](https://github.com/apache/arrow/pull/11747)
     to update GCC flags was merged
   * Resolved conflict in open
     [pull request](https://github.com/apache/arrow/pull/11205/)

### Good things

   * ... 
   
### Clarifications and possible problems

   * ... 

### Ideas to improve something

   * ... 

### Etc
   
   * ... 

</section>



<section>

## 2021-11-19 to 2021-11-21

### What was done
 
   * Update and test verifcation script to download repository 
     [pull request](https://github.com/apache/arrow/pull/11735),
   * Update GCC flags
     [pull request](https://github.com/apache/arrow/pull/11747)
   * Tried verifying Javascript release candidate.

### Good things

   * Found description of an interesting use of CMake
     https://64.github.io/cmake-raytracer/
   
### Clarifications and possible problems

   * ... 

### Ideas to improve something

   * ... 

### Etc
   
   * Topics discussed at https://www.db-tech-showcase.com/2021/ 
     mostly on database migration. Oracle track was translated.

</section>



<section>

## 2021-11-18

### What was done
 
   * Update Linux and Mac verifcation script to download repository 
     [pull request](https://github.com/apache/arrow/pull/11735),
   * Update verification instructions
     [pull request](https://github.com/apache/arrow/pull/11562)
    
### Good things

   * ... 
   
### Clarifications and possible problems

   * ... 

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>



<section>

## 2021-11-17

### What was done
 
   * Between kernel, passes all tests
     [pull request](https://github.com/apache/arrow/pull/11337),
     now need to add options for a <= b <= c, a < b <= c and a <= b < c
   * Alphabetize entries in CMakeLists file
     [pull request](https://github.com/apache/arrow/pull/11719),
     some failing tests which seem unrelated to change in file, request
     was merged
   * Conversion to unsigned types can be problematic for 
     negative numbers
     [pull request](https://github.com/apache/arrow/pull/11720)
    
### Good things

   * ... 
   
### Clarifications and possible problems

   * How to handle overflow and underflow errors in Arrow compute? Should
     one just depend on behavior of the numerical type?  What about for
     DataFusion where one might expect better handling of NANs?

### Ideas to improve something

   * May want to use flag that gives `warn_unused_result' attribute [-Werror,-Wunused-result]` 
     with GCC as it is default with Clang

### Etc
   
   * ...

</section>


<section>

## 2021-11-16

### What was done
 
   * Debugging Between kernel,

### Good things

   * ... 
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>



<section>

## 2021-11-15

### What was done
 
   * Update Between kernel,
     [Pull request](https://github.com/apache/arrow/pull/11337))

### Good things

   * ... 
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>


<section>

## 2021-11-12 to 2021-11-14

### What was done
 
   * Review RC for Arrow, Arrow-RS and DataFusion
   * Update of DataFusion verification script
     [Pull request](https://github.com/apache/arrow-datafusion/pull/1297)
   * Update verification requirements install scripts
     [Pull request](https://github.com/apache/arrow/pull/11562)
   * Update Between kernel, need to fix failing thread sanitizer build,
     [Pull request](https://github.com/apache/arrow/pull/11337))

### Good things

   * [Ruby wrapper for datafusion](https://github.com/j-a-m-l/datafusion-ruby) 
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>


<section>

## 2021-11-11

### What was done
 
   * TODO - Review RC 

### Good things

   * ... 
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>



<section>

## 2021-11-10

### What was done
 
   * ... 

### Good things

   * ... 
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>



<section>

## 2021-11-09

### What was done
 
   * ... 

### Good things

   * Learned how to version a Gem to require specific Ruby versions, >= != =>
   * Blog post on Rust release had contributed to was merged
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>




<section>

## 2021-11-08

### What was done
 
   * Fix how run Python tests on AlmaLinux,
     [Pull request](https://github.com/apache/arrow/pull/11639)
   * Continued with dependency installation script for
     running the verification script. Further review needed
     [Pull request](https://github.com/apache/arrow/pull/11562)
   * Update website with Rust 6.0.0 release blog post
     [Pull request](https://github.com/apache/arrow-site/pull/156)
   * Updating Ruby dependencies for RubyGems
     [Pull request](https://github.com/apache/arrow/pull/11637)
   * Update website to indicate tested/supported Python and Ruby
     versions
     [Pull request](https://github.com/apache/arrow-site/pull/159)

### Good things

   * ...
   
### Clarifications and possible problems

   * Ruby version dependencies for RubyGems seem to fail
     CI [Pull request](https://github.com/apache/arrow/pull/11637)
     Typical message is:
```
      There was an error parsing `Gemfile`: 
#6 1.306 [!] There was an error while loading `red-arrow.gemspec`: Illformed requirement ["=> 2.6"]. Bundler cannot continue.
```
   * While Ruby 2.5 is EOL, Ruby 2.5 is supported on RHEL 8
     Application Streams until May 2029 
     https://access.redhat.com/support/policy/updates/rhel8-app-streams-life-cycle
     How long should Ruby Arrow bindings support Ruby 2.5.9? Tests
     currently fail for this, though build succeeds. In addition to
     Ruby, want to have some policy on other languages and have related
     CI builds.

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>



<section>

## 2021-11-05 - 2021-11-07

### What was done
 
   * ... 

### Good things

   * Chance to try verification again
   
### Clarifications and possible problems

   * ... 

### Ideas to improve something

   * [Markdeep](https://casual-effects.com/markdeep/) and [Markdeep Slides](https://github.com/doersino/markdeep-slides)
     are interesting alternatives to Markdown and Asciidoc.
   * Automating some of the verification is good (e.g. for important commonly used setups), these should have specific
     dependencies set and described in the release notes, but encouraging verification on different setups would help 
     the project be more robust

### Etc
   
   * GitHub actions is not always reliable for testing. Fortunately many of the things are being moved to crossbow.
     It may be of interest to track testing hours being used on Apache Arrow.

</section>


<section>

## 2021-11-04

### What was done
 
   * ... 

### Good things

   * ...
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>


<section>

## 2021-11-03

### What was done
 
   * Worked on verification instructions
     [pull request](https://github.com/apache/arrow/pull/11562)

### Good things

   * ...
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>


<section>

## 2021-11-02

### What was done
 
   * Commented on a 
     [pull request](https://github.com/apache/arrow/pull/11593)

### Good things

   * ...
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>

<section>

## 2021-11-01

### What was done
 
   * Updated Rust release notes
     [pull request](https://github.com/apache/arrow-site/pull/156)
   * Commented on a 
     [pull request](https://github.com/apache/arrow/pull/11584)

### Good things

   * Learned a little about Rust.
   
### Clarifications and possible problems

   * ...

### Ideas to improve something

   * ... 

### Etc
   
   * ...

</section>

